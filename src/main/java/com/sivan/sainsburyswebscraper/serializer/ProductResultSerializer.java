package com.sivan.sainsburyswebscraper.serializer;

import com.google.gson.*;
import com.sivan.sainsburyswebscraper.model.ProductResult;

import java.lang.reflect.Type;

public class ProductResultSerializer implements JsonSerializer<ProductResult> {

  @Override
  public JsonElement serialize(
      ProductResult productResult, Type type, JsonSerializationContext jsonSerializationContext) {
    JsonObject jObj = (JsonObject) new GsonBuilder().create().toJsonTree(productResult);
    if (productResult.getKcalPer100g() == 0) {
      jObj.remove("kcal_per_100g");
    }
    return jObj;
  }
}
