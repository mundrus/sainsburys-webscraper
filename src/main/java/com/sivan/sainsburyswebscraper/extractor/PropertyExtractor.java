package com.sivan.sainsburyswebscraper.extractor;

import java.math.BigDecimal;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class PropertyExtractor {

  public List<String> extractProductLinks(String productListings, String baseUrl) {
    Document document = Jsoup.parse(productListings, baseUrl);
    if (document != null) {
      return document
          .body()
          .getElementsByClass("productNameAndPromotions")
          .select("h3")
          .select("a")
          .stream()
          .filter(element -> !element.getElementsByAttribute("href").isEmpty())
          .map(element -> element.absUrl("href"))
          .collect(Collectors.toList());
    }
    return Collections.emptyList();
  }

  public String extractTitle(Document document) {
    Optional<String> title =
        document
            .select(
                "#content > div.section.productContent > div.pdp > div.productSummary > div.productTitleDescriptionContainer > h1")
            .stream()
            .map(Element::text)
            .findFirst();
    return title.orElse("");
  }

  public BigDecimal extractPrice(Document document) {
    Optional<String> pricePerUnit =
        document
            .getElementsByClass("pricePerUnit")
            .stream()
            .filter(element -> !element.text().isEmpty())
            .map(Element::text)
            .findFirst();
    return new BigDecimal(cleanupNonNumericChars(pricePerUnit.orElse("0")));
  }

  public String extractDescription(Document document) {
    Optional<String> description =
        document
            .getElementsByClass("productText")
            .stream()
            .filter(element -> element.hasClass("productText"))
            .flatMap(element -> element.select("p").stream())
            .filter(element -> !StringUtils.isEmpty(element.text()))
            .map(Element::text)
            .findFirst();

    return description.orElse("");
  }

  public int extractCalories(Document document) {
    Optional<String> kCalText =
        document
            .select(".nutritionTable > tbody > tr")
            .stream()
            .filter(
                element ->
                    element.select("td").html().contains("kcal")
                        || element.select("th").html().equals("Energy kcal"))
            .flatMap(element -> element.select("td").stream())
            .map(Element::text)
            .findFirst();
    return Integer.parseInt(cleanupNonNumericChars(kCalText.orElse("0")));
  }

  private String cleanupNonNumericChars(String toBeCleaned) {
    return toBeCleaned.replaceAll("[^\\d.]", "");
  }
}
