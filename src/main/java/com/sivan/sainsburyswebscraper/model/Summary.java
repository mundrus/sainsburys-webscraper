package com.sivan.sainsburyswebscraper.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Collection;

@Data
@NoArgsConstructor
public class Summary {

  private Collection<ProductResult> results;
  private Total total;
}
