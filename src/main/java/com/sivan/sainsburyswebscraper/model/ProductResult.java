package com.sivan.sainsburyswebscraper.model;

import com.google.gson.annotations.SerializedName;
import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public class ProductResult {

  private String title;

  @Getter
  @SerializedName("kcal_per_100g")
  private int kcalPer100g;

  @Getter
  @SerializedName("unit_price")
  private BigDecimal unitPrice;

  private String description;
}
