package com.sivan.sainsburyswebscraper.model;

import java.math.BigDecimal;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class Total {

  private BigDecimal gross;
  private BigDecimal vat;
}
