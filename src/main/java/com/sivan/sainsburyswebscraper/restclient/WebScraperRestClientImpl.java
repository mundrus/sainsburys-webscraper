package com.sivan.sainsburyswebscraper.restclient;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Repository
public class WebScraperRestClientImpl implements WebScraperRestClient {

  private RestTemplate restTemplate;

  @Autowired
  public WebScraperRestClientImpl() {
    this.restTemplate = new RestTemplate();
  }

  public ResponseEntity<String> getFromSource(String sourceUrl) {
    return restTemplate.exchange(sourceUrl, HttpMethod.GET, null, String.class);
  }
}
