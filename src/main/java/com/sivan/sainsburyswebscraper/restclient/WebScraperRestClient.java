package com.sivan.sainsburyswebscraper.restclient;

import org.springframework.http.ResponseEntity;

public interface WebScraperRestClient {

  ResponseEntity<String> getFromSource(String sourceUrl);
}
