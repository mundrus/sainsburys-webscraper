package com.sivan.sainsburyswebscraper;

import com.sivan.sainsburyswebscraper.service.GrocerySiteService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Optional;

@Slf4j
@SpringBootApplication
public class SainsburysWebScraperApplication implements CommandLineRunner {

  private GrocerySiteService service;

  @Autowired
  public SainsburysWebScraperApplication(GrocerySiteService service) {
    this.service = service;
  }

  public static void main(String[] args) {
    SpringApplication.run(SainsburysWebScraperApplication.class, args);
  }

  @Override
  public void run(String... args) {
    Optional<String> result = service.getSummary();
    if (result.isPresent()) {
      log.info(result.get());
    } else {
      log.error("failed to fetch results!");
    }
  }
}
