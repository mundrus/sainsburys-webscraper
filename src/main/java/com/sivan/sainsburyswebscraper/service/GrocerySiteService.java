package com.sivan.sainsburyswebscraper.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sivan.sainsburyswebscraper.config.AppConfigProperties;
import com.sivan.sainsburyswebscraper.extractor.PropertyExtractor;
import com.sivan.sainsburyswebscraper.model.ProductResult;
import com.sivan.sainsburyswebscraper.model.Summary;
import com.sivan.sainsburyswebscraper.model.Total;
import com.sivan.sainsburyswebscraper.restclient.WebScraperRestClient;
import com.sivan.sainsburyswebscraper.serializer.ProductResultSerializer;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestClientResponseException;

@Slf4j
@Service
public class GrocerySiteService {

  private WebScraperRestClient restClient;

  private PropertyExtractor extractor;

  private AppConfigProperties configProperties;

  @Autowired
  public GrocerySiteService(
      WebScraperRestClient restClient,
      PropertyExtractor extractor,
      AppConfigProperties configProperties) {
    this.restClient = restClient;
    this.extractor = extractor;
    this.configProperties = configProperties;
  }

  public Optional<String> getSummary() {
    String baseUrl = configProperties.getBaseUrl();
    if (StringUtils.isEmpty(baseUrl)) {
      throw new IllegalArgumentException("A non-null baseUrl is required!");
    }
    ResponseEntity<String> productListingsFromSource = restClient.getFromSource(baseUrl);
    if (isInvalidResponse(productListingsFromSource)) {
      handleInvalidResponse(productListingsFromSource, baseUrl);
    }
    String productListings = productListingsFromSource.getBody();
    List<String> productLinks = extractor.extractProductLinks(productListings, baseUrl);
    Summary summary = new Summary();
    if (!productLinks.isEmpty()) {
      prepareSummary(productLinks, summary);
    }
    return prepareFormattedResult(summary);
  }

  private boolean isInvalidResponse(ResponseEntity<String> response) {
    return response.getBody() == null || !response.getStatusCode().is2xxSuccessful();
  }

  private void handleInvalidResponse(ResponseEntity<String> responseEntity, String baseUrl) {
    byte[] responseBody =
        responseEntity.getBody() != null ? responseEntity.getBody().getBytes() : null;
    throw new RestClientResponseException(
        String.format("WebScraping failed because of invalid response from %s", baseUrl),
        responseEntity.getStatusCodeValue(),
        responseEntity.getStatusCode().toString(),
        null,
        responseBody,
        Charset.defaultCharset());
  }

  private void prepareSummary(List<String> productLinks, Summary summary) {
    Collection<ProductResult> productResults = createProductResults(productLinks);
    Total total = calculateAndSetTotal(productResults);
    summary.setResults(productResults);
    summary.setTotal(total);
  }

  private Collection<ProductResult> createProductResults(List<String> productLinks) {
    return productLinks
        .stream()
        .map(this::getProductResult)
        .filter(Optional::isPresent)
        .map(Optional::get)
        .collect(Collectors.toList());
  }

  private Optional<ProductResult> getProductResult(String productLink) {
    ResponseEntity<String> productInfo = restClient.getFromSource(productLink);
    if (isInvalidResponse(productInfo)) {
      log.error("Failed to fetch ProductInfo for {}, moving on..", productLink);
      return Optional.empty();
    }
    return extractProductResult(productInfo.getBody());
  }

  private Optional<ProductResult> extractProductResult(String productInfo) {
    Document document = Jsoup.parse(productInfo);
    if (document != null) {
      String title = extractor.extractTitle(document);
      String description = extractor.extractDescription(document);
      int kCal = extractor.extractCalories(document);
      BigDecimal price = extractor.extractPrice(document);
      return Optional.of(new ProductResult(title, kCal, price, description));
    }
    return Optional.empty();
  }

  private Total calculateAndSetTotal(Collection<ProductResult> productResults) {
    BigDecimal gross =
        productResults
            .stream()
            .map(ProductResult::getUnitPrice)
            .filter(Objects::nonNull)
            .reduce(BigDecimal.ZERO, BigDecimal::add);
    BigDecimal net = gross.divide(BigDecimal.valueOf(1.2), 2, BigDecimal.ROUND_CEILING);
    BigDecimal vat = gross.subtract(net);
    return new Total(gross, vat);
  }

  private Optional<String> prepareFormattedResult(Summary summary) {
    Gson gson =
        new GsonBuilder()
            .setPrettyPrinting()
            .disableHtmlEscaping()
            .registerTypeAdapter(ProductResult.class, new ProductResultSerializer())
            .create();
    return Optional.of(gson.toJson(summary));
  }
}
