package com.sivan.sainsburyswebscraper.serializer;

import com.google.gson.JsonElement;
import com.sivan.sainsburyswebscraper.model.ProductResult;
import java.math.BigDecimal;
import org.junit.Assert;
import org.junit.Test;

public class ProductResultSerializerTest {

  private ProductResultSerializer onTest = new ProductResultSerializer();

  @Test
  public void testSerialize_WhenCaloriesIsNotZeroThenKcalShouldNotBeOmitted() {
    ProductResult productResult = new ProductResult("Title", 100, BigDecimal.valueOf(5.5), "Description");
    JsonElement jsonElement = onTest.serialize(productResult, ProductResult.class, null);
    Assert.assertEquals(
        "Unexpected Content!",
        "100",
        jsonElement.getAsJsonObject().get("kcal_per_100g").toString());
  }

  @Test
  public void testSerialize_WhenCaloriesIsZeroThenKcalShouldBeOmitted() {
    ProductResult productResult = new ProductResult("Title", 0, BigDecimal.valueOf(5.5), "Description");
    JsonElement jsonElement = onTest.serialize(productResult, ProductResult.class, null);
    Assert.assertFalse(jsonElement.getAsJsonObject().keySet().contains("kcal_per_100g"));
  }
}
