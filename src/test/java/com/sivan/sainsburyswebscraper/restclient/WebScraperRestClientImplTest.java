package com.sivan.sainsburyswebscraper.restclient;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;

@RunWith(MockitoJUnitRunner.class)
public class WebScraperRestClientImplTest {

  private static final String SOURCE_URL = "http://test-url.co.uk";

  @InjectMocks private WebScraperRestClientImpl onTest;

  @Mock private RestTemplate restTemplate;

  @Test
  public void testGetFromSource() {
    onTest.getFromSource(SOURCE_URL);
    Mockito.verify(restTemplate).exchange(SOURCE_URL, HttpMethod.GET, null, String.class);
  }
}
