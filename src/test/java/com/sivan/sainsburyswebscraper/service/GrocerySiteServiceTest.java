package com.sivan.sainsburyswebscraper.service;

import com.sivan.sainsburyswebscraper.config.AppConfigProperties;
import com.sivan.sainsburyswebscraper.extractor.PropertyExtractor;
import com.sivan.sainsburyswebscraper.restclient.WebScraperRestClientImpl;
import java.math.BigDecimal;
import org.json.JSONException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestClientResponseException;

import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;

import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class GrocerySiteServiceTest {

  private static final String URL = "https://test-url.co.uk";
  private static final String TEST_HTML = "<html></html>";

  @Rule public ExpectedException expectedException = ExpectedException.none();

  private GrocerySiteService onTest;

  @Mock private WebScraperRestClientImpl restClient;

  @Mock private PropertyExtractor extractor;

  @Before
  public void setUp() {
    onTest = new GrocerySiteService(restClient, extractor, new AppConfigProperties(URL));
    ReflectionTestUtils.setField(onTest, "configProperties", new AppConfigProperties(URL));
  }

  @Test
  public void testGetSummary_WhenBaseUrlIsNull_IllegalArgumentExceptionShouldBeThrown() {
    expectedException.expect(IllegalArgumentException.class);
    expectedException.expectMessage("A non-null baseUrl is required!");
    ReflectionTestUtils.setField(onTest, "configProperties", new AppConfigProperties(null));
    onTest.getSummary();
  }

  @Test
  public void
      testGetSummary_WhenRestCallForProductLinksFailThenAnExceptionShouldBeThrownWithCorrectHttpCode() {
    expectedException.expect(RestClientResponseException.class);
    expectedException.expectMessage("WebScraping failed because of invalid response from " + URL);
    expectedException.expect(hasProperty("rawStatusCode", is(400)));
    when(restClient.getFromSource(URL)).thenReturn(ResponseEntity.badRequest().body(null));
    onTest.getSummary();
    verify(restClient).getFromSource(URL);
  }

  @Test
  public void
      testGetSummary_WhenRestCallForProductLinksSucceedsButReturnsNoBodyThenAnExceptionShouldBeThrownWithCorrectHttpCode() {
    expectedException.expect(RestClientResponseException.class);
    expectedException.expectMessage("WebScraping failed because of invalid response from " + URL);
    expectedException.expect(hasProperty("rawStatusCode", is(200)));
    when(restClient.getFromSource(URL)).thenReturn(ResponseEntity.ok().body(null));
    onTest.getSummary();
    verify(restClient).getFromSource(URL);
  }

  @Test
  public void
      testGetSummary_WhenRestCallForProductLinksSucceedsWithNoProductLinksThenAnExceptionShouldBeThrownWithCorrectHttpCode() {
    when(restClient.getFromSource(URL)).thenReturn(ResponseEntity.ok().body(TEST_HTML));
    when(extractor.extractProductLinks(TEST_HTML, URL)).thenReturn(Collections.emptyList());
    Optional<String> result = onTest.getSummary();
    verify(restClient).getFromSource(URL);
    assertTrue(result.isPresent());
    assertEquals("Unexpected Result!", "{}", result.get());
  }

  @Test
  public void
      testGetSummary_WhenMultipleProductLinksArePresentInResponseThenRestCallsShouldBePlacedForEachLink() {
    when(restClient.getFromSource(anyString())).thenReturn(ResponseEntity.ok().body(TEST_HTML));
    when(extractor.extractProductLinks(TEST_HTML, URL))
        .thenReturn(Arrays.asList("ProductLink1", "ProductLink2"));
    onTest.getSummary();
    verify(restClient).getFromSource("ProductLink1");
    verify(restClient).getFromSource("ProductLink2");
  }

  @Test
  public void
      testGetSummary_WhenProductInfoIsNotAvailableForOneProductThenRestExtractionShouldBeContinuedForRest() {
    when(restClient.getFromSource(URL)).thenReturn(ResponseEntity.ok(TEST_HTML));
    when(restClient.getFromSource("ProductLink1")).thenReturn(ResponseEntity.ok(TEST_HTML));
    when(restClient.getFromSource("ProductLink2")).thenReturn(ResponseEntity.badRequest().build());
    when(extractor.extractProductLinks(TEST_HTML, URL))
        .thenReturn(Arrays.asList("ProductLink1", "ProductLink2"));
    onTest.getSummary();
    verify(restClient).getFromSource("ProductLink1");
    verify(restClient).getFromSource("ProductLink2");
  }

  @Test
  public void testGetSummary_WhenProductInfoIsAvailableThenExtractionOfPropertiesShouldBeAttempted() {
    when(restClient.getFromSource(anyString())).thenReturn(ResponseEntity.ok().body(TEST_HTML));
    when(extractor.extractProductLinks(TEST_HTML, URL))
        .thenReturn(Collections.singletonList("ProductLink1"));
    onTest.getSummary();
    Document document = Jsoup.parse(TEST_HTML, URL);
    ArgumentCaptor<Document> captor = ArgumentCaptor.forClass(Document.class);
    verify(extractor).extractTitle(captor.capture());
    verify(extractor).extractDescription(captor.capture());
    verify(extractor).extractPrice(captor.capture());
    verify(extractor).extractCalories(captor.capture());
    assertTrue(
        captor
            .getAllValues()
            .stream()
            .allMatch(value -> value.toString().equals(document.toString())));
  }

  @Test
  public void testGetSummary_WhenPropertiesAreExtractedThenPreparedSummaryShouldBeReturned()
      throws JSONException {
    when(restClient.getFromSource(anyString())).thenReturn(ResponseEntity.ok().body(TEST_HTML));
    when(extractor.extractProductLinks(TEST_HTML, URL))
        .thenReturn(Collections.singletonList("ProductLink1"));
    when(extractor.extractTitle(any(Document.class))).thenReturn("Title");
    when(extractor.extractDescription(any(Document.class))).thenReturn("Description");
    when(extractor.extractPrice(any(Document.class))).thenReturn(BigDecimal.TEN);
    when(extractor.extractCalories(any(Document.class))).thenReturn(50);
    Optional<String> summary = onTest.getSummary();
    assertTrue(summary.isPresent());
    JSONAssert.assertEquals(
        "{\n"
            + "  \"results\": [\n"
            + "    {\n"
            + "      \"title\": \"Title\",\n"
            + "      \"kcal_per_100g\": 50,\n"
            + "      \"unit_price\": 10.0,\n"
            + "      \"description\": \"Description\"\n"
            + "    }\n"
            + "  ],\n"
            + "  \"total\": {\n"
            + "    \"gross\": 10.0,\n"
            + "    \"vat\": 1.66\n"
            + "  }\n"
            + "}",
        summary.get(),
        JSONCompareMode.STRICT);
  }

  @Test
  public void testGetSummary_WhenCaloriesFigureIsZeroThenPreparedSummaryShouldOmitTheJsonProperty()
      throws JSONException {
    when(restClient.getFromSource(anyString())).thenReturn(ResponseEntity.ok().body(TEST_HTML));
    when(extractor.extractProductLinks(TEST_HTML, URL))
        .thenReturn(Collections.singletonList("ProductLink1"));
    when(extractor.extractTitle(any(Document.class))).thenReturn("Title");
    when(extractor.extractDescription(any(Document.class))).thenReturn("Description");
    when(extractor.extractPrice(any(Document.class))).thenReturn(BigDecimal.TEN);
    when(extractor.extractCalories(any(Document.class))).thenReturn(0);
    Optional<String> summary = onTest.getSummary();
    assertTrue(summary.isPresent());
    JSONAssert.assertEquals(
        "{\n"
            + "  \"results\": [\n"
            + "    {\n"
            + "      \"title\": \"Title\",\n"
            + "      \"unit_price\": 10.0,\n"
            + "      \"description\": \"Description\"\n"
            + "    }\n"
            + "  ],\n"
            + "  \"total\": {\n"
            + "    \"gross\": 10.0,\n"
            + "    \"vat\": 1.66\n"
            + "  }\n"
            + "}",
        summary.get(),
        JSONCompareMode.STRICT);
  }
}
