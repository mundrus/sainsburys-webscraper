package com.sivan.sainsburyswebscraper.service;

import com.sivan.sainsburyswebscraper.SainsburysWebScraperApplication;
import com.sivan.sainsburyswebscraper.config.TestConfig;
import org.json.JSONException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {SainsburysWebScraperApplication.class, TestConfig.class})
public class GrocerySiteServiceIntegrationTest {

  @Autowired private GrocerySiteService onTest;

  @Test
  public void testGetResult() throws JSONException {
    String result =
        "{\n"
            + "  \"results\": [\n"
            + "    {\n"
            + "      \"title\": \"Sainsbury's Strawberries 400g\",\n"
            + "      \"kcal_per_100g\": 33,\n"
            + "      \"unit_price\": 1.75,\n"
            + "      \"description\": \"by Sainsbury's strawberries\"\n"
            + "    },\n"
            + "    {\n"
            + "      \"title\": \"Sainsbury's Blueberries 200g\",\n"
            + "      \"kcal_per_100g\": 45,\n"
            + "      \"unit_price\": 1.75,\n"
            + "      \"description\": \"by Sainsbury's blueberries\"\n"
            + "    },\n"
            + "    {\n"
            + "      \"title\": \"Sainsbury's Raspberries 225g\",\n"
            + "      \"kcal_per_100g\": 32,\n"
            + "      \"unit_price\": 1.75,\n"
            + "      \"description\": \"by Sainsbury's raspberries\"\n"
            + "    },\n"
            + "    {\n"
            + "      \"title\": \"Sainsbury's Blackberries, Sweet 150g\",\n"
            + "      \"kcal_per_100g\": 32,\n"
            + "      \"unit_price\": 1.5,\n"
            + "      \"description\": \"by Sainsbury's blackberries\"\n"
            + "    },\n"
            + "    {\n"
            + "      \"title\": \"Sainsbury's Blueberries 400g\",\n"
            + "      \"kcal_per_100g\": 45,\n"
            + "      \"unit_price\": 3.25,\n"
            + "      \"description\": \"by Sainsbury's blueberries\"\n"
            + "    },\n"
            + "    {\n"
            + "      \"title\": \"Sainsbury's Blueberries, SO Organic 150g\",\n"
            + "      \"kcal_per_100g\": 45,\n"
            + "      \"unit_price\": 2.0,\n"
            + "      \"description\": \"So Organic blueberries\"\n"
            + "    },\n"
            + "    {\n"
            + "      \"title\": \"Sainsbury's Raspberries, Taste the Difference 150g\",\n"
            + "      \"kcal_per_100g\": 32,\n"
            + "      \"unit_price\": 2.5,\n"
            + "      \"description\": \"Ttd raspberries\"\n"
            + "    },\n"
            + "    {\n"
            + "      \"title\": \"Sainsbury's Cherries 400g\",\n"
            + "      \"kcal_per_100g\": 52,\n"
            + "      \"unit_price\": 2.5,\n"
            + "      \"description\": \"by Sainsbury's Family Cherry Punnet\"\n"
            + "    },\n"
            + "    {\n"
            + "      \"title\": \"Sainsbury's Blackberries, Tangy 150g\",\n"
            + "      \"kcal_per_100g\": 32,\n"
            + "      \"unit_price\": 1.5,\n"
            + "      \"description\": \"by Sainsbury's blackberries\"\n"
            + "    },\n"
            + "    {\n"
            + "      \"title\": \"Sainsbury's Strawberries, Taste the Difference 300g\",\n"
            + "      \"kcal_per_100g\": 33,\n"
            + "      \"unit_price\": 2.5,\n"
            + "      \"description\": \"Ttd strawberries\"\n"
            + "    },\n"
            + "    {\n"
            + "      \"title\": \"Sainsbury's Cherry Punnet 200g\",\n"
            + "      \"kcal_per_100g\": 52,\n"
            + "      \"unit_price\": 1.5,\n"
            + "      \"description\": \"Cherries\"\n"
            + "    },\n"
            + "    {\n"
            + "      \"title\": \"Sainsbury's Mixed Berries 300g\",\n"
            + "      \"unit_price\": 3.5,\n"
            + "      \"description\": \"by Sainsbury's mixed berries\"\n"
            + "    },\n"
            + "    {\n"
            + "      \"title\": \"Sainsbury's Mixed Berry Twin Pack 200g\",\n"
            + "      \"unit_price\": 2.75,\n"
            + "      \"description\": \"Mixed Berries\"\n"
            + "    },\n"
            + "    {\n"
            + "      \"title\": \"Sainsbury's Redcurrants 150g\",\n"
            + "      \"kcal_per_100g\": 71,\n"
            + "      \"unit_price\": 2.5,\n"
            + "      \"description\": \"by Sainsbury's redcurrants\"\n"
            + "    },\n"
            + "    {\n"
            + "      \"title\": \"Sainsbury's Cherry Punnet, Taste the Difference 200g\",\n"
            + "      \"kcal_per_100g\": 48,\n"
            + "      \"unit_price\": 2.5,\n"
            + "      \"description\": \"Cherry Punnet\"\n"
            + "    },\n"
            + "    {\n"
            + "      \"title\": \"Sainsbury's Blackcurrants 150g\",\n"
            + "      \"unit_price\": 1.75,\n"
            + "      \"description\": \"Union Flag\"\n"
            + "    },\n"
            + "    {\n"
            + "      \"title\": \"Sainsbury's British Cherry & Strawberry Pack 600g\",\n"
            + "      \"unit_price\": 4.0,\n"
            + "      \"description\": \"British Cherry & Strawberry Mixed Pack\"\n"
            + "    }\n"
            + "  ],\n"
            + "  \"total\": {\n"
            + "    \"gross\": 39.5,\n"
            + "    \"vat\": 6.58\n"
            + "  }\n"
            + "}";
    Optional<String> resultOptional = onTest.getSummary();
    Assert.assertTrue(resultOptional.isPresent());
    JSONAssert.assertEquals(result, resultOptional.get(), JSONCompareMode.STRICT);
  }
}
