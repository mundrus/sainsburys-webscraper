package com.sivan.sainsburyswebscraper.config;

import com.sivan.sainsburyswebscraper.service.GrocerySiteService;
import org.mockito.Mockito;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;

@TestConfiguration
public class TestConfig {

  @Bean
  @Primary
  @Profile("test")
  public GrocerySiteService testGroceryService() {
    return Mockito.mock(GrocerySiteService.class);
  }
}
