package com.sivan.sainsburyswebscraper.extractor;

import java.math.BigDecimal;
import org.jsoup.Jsoup;
import org.junit.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class PropertyExtractorTest {

  private static final String TEST_URL = "https://test-url.co.uk";

  private PropertyExtractor onTest = new PropertyExtractor();

  @Test
  public void testExtractProductLinks_WhenProductLinksAreFoundThenTheyShouldBeReturned()
      throws URISyntaxException, IOException {
    List<String> expectedProductLinks =
        Arrays.asList(
            "https://test-url.co.uk/../../shop/gb/groceries/berries-cherries-currants/sainsburys-british-strawberries-400g.html",
            "https://test-url.co.uk/../../shop/gb/groceries/berries-cherries-currants/sainsburys-blueberries-200g.html",
            "https://test-url.co.uk/../../shop/gb/groceries/berries-cherries-currants/sainsburys-raspberries-225g.html",
            "https://test-url.co.uk/../../shop/gb/groceries/berries-cherries-currants/sainsburys-blackberries--sweet-150g.html",
            "https://test-url.co.uk/../../shop/gb/groceries/berries-cherries-currants/sainsburys-blueberries-400g.html",
            "https://test-url.co.uk/../../shop/gb/groceries/berries-cherries-currants/sainsburys-blueberries--so-organic-150g.html",
            "https://test-url.co.uk/../../shop/gb/groceries/berries-cherries-currants/sainsburys-raspberries--taste-the-difference-150g.html",
            "https://test-url.co.uk/../../shop/gb/groceries/berries-cherries-currants/sainsburys-cherries-350g.html",
            "https://test-url.co.uk/../../shop/gb/groceries/berries-cherries-currants/sainsburys-blackberries--tangy-150g.html",
            "https://test-url.co.uk/../../shop/gb/groceries/berries-cherries-currants/sainsburys-strawberries--taste-the-difference-300g.html",
            "https://test-url.co.uk/../../shop/gb/groceries/berries-cherries-currants/sainsburys-cherry-punnet-200g-468015-p-44.html",
            "https://test-url.co.uk/../../shop/gb/groceries/berries-cherries-currants/sainsburys-mixed-berries-300g.html",
            "https://test-url.co.uk/../../shop/gb/groceries/berries-cherries-currants/sainsburys-mixed-berry-twin-pack-200g-7696255-p-44.html",
            "https://test-url.co.uk/../../shop/gb/groceries/berries-cherries-currants/sainsburys-redcurrants-150g.html",
            "https://test-url.co.uk/../../shop/gb/groceries/berries-cherries-currants/sainsburys-cherry-punnet--taste-the-difference-250g.html",
            "https://test-url.co.uk/../../shop/gb/groceries/berries-cherries-currants/sainsburys-blackcurrants-150g.html",
            "https://test-url.co.uk/../../shop/gb/groceries/berries-cherries-currants/sainsburys-british-cherry---strawberry-pack-600g.html");
    Path path = Paths.get(getClass().getClassLoader().getResource("landing-page.html").toURI());
    String content = new String(Files.readAllBytes(path));
    List<String> productLinks = onTest.extractProductLinks(content, TEST_URL);
    assertEquals("Unexpected Content!", expectedProductLinks, productLinks);
  }

  @Test
  public void testExtractProductLinks_WhenProductLinksAreNotFoundThenEmptyListShouldBeReturned() {
    List<String> productLinks = onTest.extractProductLinks("<html></html>", TEST_URL);
    assertEquals("Unexpected Content!", Collections.emptyList(), productLinks);
  }

  @Test
  public void testExtractTitle_WhenTitleIsFoundThenItShouldBeReturned()
      throws IOException, URISyntaxException {
    Path path = Paths.get(getClass().getClassLoader().getResource("product-page-single-line-description.html").toURI());
    String content = new String(Files.readAllBytes(path));
    String title = onTest.extractTitle(Jsoup.parse(content));
    assertEquals("Unexpected Content!", "Sainsbury's Strawberries 400g", title);
  }

  @Test
  public void testExtractTitle_WhenTitleIsNotFoundThenEmptyStringShouldBeReturn() {
    String title = onTest.extractTitle(Jsoup.parse("<html></html>"));
    assertEquals("Unexpected Content!", "", title);
  }

  @Test
  public void testExtractPrice_WhenPriceIsFoundThenParsedDoubleValueOfPriceShouldBeReturned()
      throws IOException, URISyntaxException {
    Path path = Paths.get(getClass().getClassLoader().getResource("product-page-single-line-description.html").toURI());
    String content = new String(Files.readAllBytes(path));
    BigDecimal price = onTest.extractPrice(Jsoup.parse(content));
    assertEquals("Unexpected Content!", BigDecimal.valueOf(1.75), price);
  }

  @Test
  public void testExtractPrice_WhenPriceElementIsNotFoundThenZeroShouldBeReturned() {
    BigDecimal price = onTest.extractPrice(Jsoup.parse("<html></html>"));
    assertEquals("Unexpected Content!", BigDecimal.ZERO, price);
  }

  @Test
  public void testExtractPrice_WhenPriceTextIsNotFoundThenZeroShouldBeReturned() {
    BigDecimal price =
        onTest.extractPrice(Jsoup.parse("<html><div class=\"pricePerUnit\"></div></html>"));
    assertEquals("Unexpected Content!", BigDecimal.ZERO, price);
  }

  @Test
  public void testExtractDescription_ProductWithSingleLineDescription_WhenDescriptionIsFoundThenShouldBeReturned()
      throws IOException, URISyntaxException {
    Path path = Paths.get(getClass().getClassLoader().getResource("product-page-single-line-description.html").toURI());
    String content = new String(Files.readAllBytes(path));
    String description = onTest.extractDescription(Jsoup.parse(content));
    assertEquals("Unexpected Content!", "by Sainsbury's strawberries", description);
  }

  @Test
  public void testExtractDescription_ProductWithMultiLineDescription_WhenDescriptionIsFoundThenShouldBeReturned()
          throws IOException, URISyntaxException {
    Path path = Paths.get(getClass().getClassLoader().getResource("product-page-multi-line-description.html").toURI());
    String content = new String(Files.readAllBytes(path));
    String description = onTest.extractDescription(Jsoup.parse(content));
    assertEquals("Unexpected Content!", "Cherries", description);
  }

  @Test
  public void testExtractDescription_WhenDescriptionNotFoundThenEmptyStringShouldBeReturned() {
    String description = onTest.extractDescription(Jsoup.parse("<html></html>"));
    assertEquals("Unexpected Content!", "", description);
  }

  @Test
  public void testExtractCalories_ProductWithKcalInText_WhenCaloriesFoundThenShouldBeReturned() throws IOException, URISyntaxException {
    Path path = Paths.get(getClass().getClassLoader().getResource("product-page-kcal-in-text.html").toURI());
    String content = new String(Files.readAllBytes(path));
    int calories = onTest.extractCalories(Jsoup.parse(content));
    assertEquals("Unexpected Content!", 33, calories);
  }

  @Test
  public void testExtractCalories_ProductWithKcalInHeader_WhenCaloriesFoundThenShouldBeReturned() throws IOException, URISyntaxException {
    Path path = Paths.get(getClass().getClassLoader().getResource("product-page-kcal-in-header.html").toURI());
    String content = new String(Files.readAllBytes(path));
    int calories = onTest.extractCalories(Jsoup.parse(content));
    assertEquals("Unexpected Content!", 52, calories);
  }

  @Test
  public void testExtractCalories_WhenDescriptionNotFoundThenEmptyStringShouldBeReturned() {
    int calories = onTest.extractCalories(Jsoup.parse("<html></html>"));
    assertEquals("Unexpected Content!", 0, calories);
  }
}
