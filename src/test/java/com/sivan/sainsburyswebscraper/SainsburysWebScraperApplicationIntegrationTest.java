package com.sivan.sainsburyswebscraper;

import com.sivan.sainsburyswebscraper.config.TestConfig;
import com.sivan.sainsburyswebscraper.service.GrocerySiteService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {SainsburysWebScraperApplication.class, TestConfig.class})
@ActiveProfiles("test")
public class SainsburysWebScraperApplicationIntegrationTest {

  @Autowired private GrocerySiteService service;

  @Test
  public void testContextLoadsAndServiceIsCalled() {
    Mockito.verify(service).getSummary();
  }
}
