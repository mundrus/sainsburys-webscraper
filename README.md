#Sainsburys Grocery Site WebScraper

###Scope:
This is intended to be a complete solution for task specified at https://jsainsburyplc.github.io/serverside-test/.

###Prerequisites to run the app:
Java (1.8) and Maven

###To run tests:
`mvn test`

###To build:
`mvn clean install`

###To run the app:
`java -jar target/sainsburys-webscraper-0.0.1-SNAPSHOT.jar`

